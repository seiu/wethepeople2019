<?php

function elementor_widget_list() {
    $avaliable_widgets = array();
    foreach (glob(get_template_directory() . '/inc/elementor/includes/addons/*.php') as $file) {
        $data = get_file_data($file, array('class' => 'Class', 'name' => 'Name', 'slug' => 'Slug'));

        $slug = basename($file, '.php');
        $avaliable_widgets[$slug] = $data['name'];
    }
    return $avaliable_widgets;
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 */
function optionsframework_options() {

    $widget_list = total_plus_widget_list();
    $elementor_widget_list = elementor_widget_list();
    $std_widget_list = $std_elementor_widget_list = array();

    foreach ($widget_list as $key => $widget) {
        $std_widget_list[$key] = '1';
    }

    foreach ($elementor_widget_list as $key => $widget) {
        $std_elementor_widget_list[$key] = '1';
    }

    $options = array();

    $options[] = array(
        'name' => __('Customizer Settings', 'total-plus'),
        'type' => 'heading');

    $options[] = array(
        'name' => __('Choose Icon Sets', 'total-plus'),
        'desc' => __('These icon set will appear in the <a target="_blank" href="' . admin_url('customize.php?autofocus[panel]=total_plus_home_panel') . '">Home Page Sections</a>. Choose the set of icons that you want to use. Choosing all the icon set may slow down the customizer panel.', 'total-plus'),
        'id' => 'customizer_icon_sets',
        'std' => array(
            'ico_font' => '1',
            'font_awesome' => '1',
            'essential_icon' => '1',
            'material_icon' => '1'
        ),
        'type' => 'multicheck',
        'options' => array(
            'ico_font' => __('Icon Font - <a href="https://icofont.com/icons" target="_blank">View Here</a>', 'total-plus'),
            'font_awesome' => __('Font Awesome - <a href="https://fontawesome.com/icons?m=free" target="_blank">View Here</a>', 'total-plus'),
            'essential_icon' => __('Essential Light Icons - <a href="http://www.essential-icons.com/" target="_blank">View Here</a>', 'total-plus'),
            'material_icon' => __('Material Icons - <a href="http://materialdesignicons.com/" target="_blank">View Here</a>', 'total-plus'),
        )
    );

    $options[] = array(
        'type' => 'break'
    );

    $options[] = array(
        'name' => __('Maintenance Mode Panel', 'total-plus'),
        'label' => __('Enable/Disable', 'total-plus'),
        'desc' => __('If you are not using <a target="_blank" href="' . admin_url('customize.php?autofocus[section]=total_plus_maintenance_section') . '">Maintenance Screen</a> then disabling can increase the loading speed of customizer panel.', 'total-plus'),
        'id' => 'customizer_maintenance_mode',
        'std' => '1',
        'type' => 'checkbox'
    );

    $options[] = array(
        'type' => 'break'
    );

    $options[] = array(
        'name' => __('GDPR Settings Panel', 'total-plus'),
        'label' => __('Enable/Disable', 'total-plus'),
        'desc' => __('If you are not using <a target="_blank" href="' . admin_url('customize.php?autofocus[section]=total_plus_gdpr_section') . '">GDPR Option</a> then disabling can increase the loading speed of customizer panel.', 'total-plus'),
        'id' => 'customizer_gdpr_settings',
        'std' => '1',
        'type' => 'checkbox'
    );

    $options[] = array(
        'type' => 'break'
    );

    $options[] = array(
        'name' => __('Disable Home Page Settings Panel', 'total-plus'),
        'label' => __('Enable/Disable', 'total-plus'),
        'desc' => __('If you are using page builder instead of <a target="_blank" href="' . admin_url('customize.php?autofocus[panel]=total_plus_home_panel') . '">Customizer Home Page Sections</a> then disabling can increase the loading speed of customizer panel.', 'total-plus'),
        'id' => 'customizer_home_settings',
        'std' => '1',
        'type' => 'checkbox'
    );

    $options[] = array(
        'name' => __('Widget Settings', 'total-plus'),
        'type' => 'heading'
    );

    $options[] = array(
        'name' => __('Widgets', 'total-plus'),
        'desc' => __('Enable/Disable the Widgets. This widgets will show in <a target="_blank" href="'.admin_url('/widgets.php').'">Widget Page</a> and SiteOrigin Page Builder if you have installed and activated <a target="_blank" href="https://wordpress.org/plugins/siteorigin-panels/">SiteOrigin Page Builder</a>', 'total-plus'),
        'id' => 'enabled_widgets',
        'std' => $std_widget_list,
        'type' => 'multicheck',
        'class' => 'three-col-multicheck',
        'options' => $widget_list
    );

    $options[] = array(
        'name' => __('Elementor Widget Settings', 'total-plus'),
        'type' => 'heading'
    );

    $options[] = array(
        'name' => __('Available Elementor Widgets', 'total-plus'),
        'desc' => __('List of widgets that will be available when editing the page with Elementor.', 'total-plus'),
        'id' => 'avaliable_widgets',
        'std' => $std_elementor_widget_list,
        'type' => 'multicheck',
        'class' => 'three-col-multicheck',
        'options' => elementor_widget_list()
    );

    $options[] = array(
        'name' => __('Use Jet Templates', 'total-plus'),
        'desc' => __('Add Jet page templates and blocks to Elementor templates library.', 'total-plus'),
        'id' => 'jet_templates',
        'type' => 'checkbox',
        'label' => __('Enable/Disable', 'total-plus'),
    );

    $options[] = array(
        'type' => 'break'
    );

    $options[] = array(
        'name' => __('NOTE', 'total-plus'),
        'desc' => __('These settings will work only if you have installed and activated the <a target="_blank" href="https://wordpress.org/plugins/elementor/">Elementor</a> Plugin. You can install Elementor Plugin <a href="' . admin_url('/admin.php?page=total-plus-recommended-plugins') . '">here</a>.', 'total-plus'),
        'type' => 'info',
        'class' => 'boxed-note'
    );

    $options[] = array(
        'name' => __('API Keys', 'total-plus'),
        'type' => 'heading'
    );

    $options[] = array(
        'name' => __('Google Map API Key', 'total-plus'),
        'desc' => __('Create own API key. <a target="_blank" href="https://hashthemes.com/articles/creating-a-google-maps-api-key/">Guide on creating Google Maps API Key</a>', 'total-plus'),
        'id' => 'api_key',
        'type' => 'text'
    );

    $options[] = array(
        'type' => 'break'
    );

    $options[] = array(
        'name' => __('Instagram Access Token:', 'total-plus'),
        'desc' => __('Read more about how to get Instagram Access Token <a target="_blank" href="https://elfsight.com/blog/2016/05/how-to-get-instagram-access-token/">here</a> or <a target="_blank" href="https://docs.oceanwp.org/article/487-how-to-get-instagram-access-token">here</a>', 'total-plus'),
        'id' => 'insta_access_token',
        'type' => 'text'
    );

    return $options;
}
