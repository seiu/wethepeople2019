<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Jet_Elements_Mp_Timetable extends Jet_Elements_Base {

	public function get_name() {
		return 'mp-timetable';
	}

	public function get_title() {
		return esc_html__( 'Timetable by MotoPress', 'total-plus' );
	}

	public function get_icon() {
		return 'eicon-table';
	}

	public function get_categories() {
		return array( 'cherry' );
	}

	public function __tag() {
		return 'mp-timetable';
	}

	public function get_script_depends() {

		if ( ! isset( $_GET['elementor-preview'] ) ) {
			return array();
		}

		$core = \mp_timetable\plugin_core\classes\Core::get_instance();

		wp_register_script(
			'mptt-event-object',
			\Mp_Time_Table::get_plugin_url( 'media/js/events/event' . $core->get_prefix() . '.js' ),
			array( 'jquery' ),
			$core->get_version()
		);

		wp_register_script(
			'mptt-functions',
			\Mp_Time_Table::get_plugin_url( 'media/js/mptt-functions' . $core->get_prefix() . '.js' ),
			array( 'jquery', 'underscore' ),
			$core->get_version()
		);

		wp_localize_script(
			'mptt-event-object',
			'MPTT',
			array(
				'table_class' => apply_filters( 'mptt_shortcode_static_table_class', 'mptt-shortcode-table' ),
			)
		);

		return array( 'mptt-functions', 'mptt-event-object' );
	}

	/**
	 * @param array $data_array
	 * @param string $type
	 *
	 * @return array
	 */
	public function __create_list( $data_array = array(), $type = 'post' ) {
		$list_array = array();
		switch ( $type ) {
			case "post":
				foreach ( $data_array as $item ) {
					$list_array[ $item->ID ] = $item->post_title;
				}
				break;
			case "term":
				foreach ( $data_array as $item ) {
					$list_array[ $item->term_id ] = $item->name;
				}
				break;
			default:
				break;
		}

		return $list_array;
	}

	public function __atts() {

		$columns    = $this->__create_list( \mp_timetable\classes\models\Column::get_instance()->get_all_column() );
		$events     = $this->__create_list( \mp_timetable\classes\models\Events::get_instance()->get_all_events() );
		$categories = get_terms( 'mp-event_category', 'orderby=count&hide_empty=0' );
		$categories = $this->__create_list( $categories, 'term' );

		return array(
			'col' => array(
				'type'     => Controls_Manager::SELECT2,
				'label'    => __( 'Column', 'total-plus' ),
				'multiple' => true,
				'options'  => $columns,
			),
			'events' => array(
				'type'     => Controls_Manager::SELECT2,
				'label'    => __( 'Events', 'total-plus' ),
				'multiple' => true,
				'options'  => $events,
			),
			'event_categ'       => array(
				'type'     => Controls_Manager::SELECT2,
				'label'    => __( 'Event categories', 'total-plus' ),
				'multiple' => true,
				'options'  => $categories,
			),
			'increment' => array(
				'type'    => Controls_Manager::SELECT,
				'label'   => __( 'Hour measure', 'total-plus' ),
				'default' => '1',
				'options' => array(
					'1'    => __( 'Hour (1h)', 'total-plus' ),
					'0.5'  => __( 'Half hour (30min)', 'total-plus' ),
					'0.25' => __( 'Quarter hour (15min)', 'total-plus' ),
				),
			),
			'view' => array(
				'type'    => Controls_Manager::SELECT,
				'label'   => __( 'Filter style', 'total-plus' ),
				'default' => 'dropdown_list',
				'options' => array(
					'dropdown_list' => __( 'Dropdown list', 'total-plus' ),
					'tabs' => __( 'Tabs', 'total-plus' ),
				),
			),
			'label' => array(
				'type'    => Controls_Manager::TEXT,
				'label'   => __( 'Filter label', 'total-plus' ),
				'default' => __( 'All Events', 'total-plus' ),
			),
			'hide_label'        => array(
				'type'    => Controls_Manager::SELECT,
				'label'   => __( "Hide 'All Events' view", 'total-plus' ),
				'default' => '0',
				'options' => array(
					'0' => __( 'No', 'total-plus' ),
					'1' => __( 'Yes', 'total-plus' ),
				),
			),
			'hide_hrs' => array(
				'type'    => Controls_Manager::SELECT,
				'label'   => __( 'Hide first (hours) column', 'total-plus' ),
				'default' => '0',
				'options' => array(
					'0' => __( 'No', 'total-plus' ),
					'1' => __( 'Yes', 'total-plus' ),
				),
			),
			'hide_empty_rows' => array(
				'type'    => Controls_Manager::SELECT,
				'label'   => __( 'Hide empty rows', 'total-plus' ),
				'default' => '1',
				'options' => array(
					'1' => __( 'Yes', 'total-plus' ),
					'0' => __( 'No', 'total-plus' ),
				),
				'default' => 1,
			),
			'title' => array(
				'type'    => Controls_Manager::SELECT,
				'label'   => __( 'Title', 'total-plus' ),
				'default' => 1,
				'options' => array(
					'1' => __( 'Yes', 'total-plus' ),
					'0' => __( 'No', 'total-plus' ),
				),
			),
			'time' => array(
				'type'    => Controls_Manager::SELECT,
				'label'   => __( 'Time', 'total-plus' ),
				'default' => 1,
				'options' => array(
					'1' => __( 'Yes', 'total-plus' ),
					'0' => __( 'No', 'total-plus' ),
				),
			),
			'sub-title' => array(
				'type'    => Controls_Manager::SELECT,
				'label'   => __( 'Subtitle', 'total-plus' ),
				'default' => 1,
				'options' => array(
					'1' => __( 'Yes', 'total-plus' ),
					'0' => __( 'No', 'total-plus' ),
				),
			),
			'description' => array(
				'type'    => Controls_Manager::SELECT,
				'label'   => __( 'Description', 'total-plus' ),
				'default' => 0,
				'options' => array(
					'1' => __( 'Yes', 'total-plus' ),
					'0' => __( 'No', 'total-plus' ),
				),
			),
			'user' => array(
				'type'    => Controls_Manager::SELECT,
				'label'   => __( 'User', 'total-plus' ),
				'default' => 0,
				'options' => array(
					'1' => __( 'Yes', 'total-plus' ),
					'0' => __( 'No', 'total-plus' ),
				),
			),
			'disable_event_url' => array(
				'type'    => Controls_Manager::SELECT,
				'label'   => __( 'Disable event URL', 'total-plus' ),
				'default' => '0',
				'options' => array(
					'0' => __( 'No', 'total-plus' ),
					'1' => __( 'Yes', 'total-plus' ),
				),
			),
			'text_align' => array(
				'type'    => Controls_Manager::SELECT,
				'label'   => __( 'Text align', 'total-plus' ),
				'default' => 'center',
				'options' => array(
					'center' => __( 'center', 'total-plus' ),
					'left'   => __( 'left', 'total-plus' ),
					'right'  => __( 'right', 'total-plus' ),
				),
			),
			'css_id' => array(
				'type'  => Controls_Manager::TEXT,
				'label' => __( 'Id', 'total-plus' )
			),
			'row_height' => array(
				'type'    => Controls_Manager::TEXT,
				'label'   => __( 'Row height (in px)', 'total-plus' ),
				'default' => 45
			),
			'font_size' => array(
				'type'    => Controls_Manager::TEXT,
				'label'   => __( 'Base Font Size', 'total-plus' ),
				'default' => ''
			),
			'responsive' => array(
				'type'    => Controls_Manager::SELECT,
				'label'   => __( 'Responsive', 'total-plus' ),
				'default' => '1',
				'options' => array(
					'1' => __( 'Yes', 'total-plus' ),
					'0' => __( 'No', 'total-plus' ),
				),
				'default' => 1,
			)
		);
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_settings',
			array(
				'label' => esc_html__( 'Settings', 'total-plus' ),
			)
		);

		foreach ( $this->__atts() as $control => $data ) {
			$this->add_control( $control, $data );
		}

		$this->end_controls_section();
	}

	protected function render() {

		$settings = $this->get_settings();

		$this->__context = 'render';

		$this->__open_wrap();

		$attributes = '';

		foreach ( $this->__atts() as $attr => $data ) {

			$attr_val    = $settings[ $attr ];
			$attr_val    = ! is_array( $attr_val ) ? $attr_val : implode( ',', $attr_val );

			if ( 'css_id' === $attr ) {
				$attr = 'id';
			}

			$attributes .= sprintf( ' %1$s="%2$s"', $attr, $attr_val );
		}

		$shortcode = sprintf( '[%s %s]', $this->__tag(), $attributes );
		echo do_shortcode( $shortcode );

		$this->__close_wrap();

	}

}
