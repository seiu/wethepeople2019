<?php
/*
Template Name: Custom Home Page for After April 1
Description: Hard coded home page for after Day of Action
*/
?>
 

<!DOCTYPE html>
<html>
  <head>
      <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-136671218-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-136671218-1');
</script>

      
      
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>We The People</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css">
<!--    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>-->
      
      <link rel="icon" href="https://wethepeople2019.org/wp-content/uploads/2019/03/cropped-wethepeoplelogo-icon-32x32.png" sizes="32x32" />
<link rel="icon" href="https://wethepeople2019.org/wp-content/uploads/2019/03/cropped-wethepeoplelogo-icon-192x192.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="https://wethepeople2019.org/wp-content/uploads/2019/03/cropped-wethepeoplelogo-icon-180x180.png" />
<meta name="msapplication-TileImage" content="https://wethepeople2019.org/wp-content/uploads/2019/03/cropped-wethepeoplelogo-icon-270x270.png" />
      
      

<meta property="og:url"                content="https://wethepeople2019.org/" />
<meta property="og:type"               content="website" />
<meta property="og:title"              content="We The People Membership Summit" />
<meta property="og:description"        content="April 1st Americans across the nation will have the opportunity to hear from some of the most important voices on the political and public policy stage" />
<meta property="og:image"              content="https://wethepeople2019.org/wp-content/uploads/2019/03/We-The-People.png" />
      
      
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@SEIU" />
      
      
      
<style>
    
.ht-header {background-color: #008bd0 !important; }
    
    
/* youtube embed responsive */       
.video-media-youtube {
  width: 100%;
  max-width: 1400px;
  margin: 0 auto; }
.video-media-youtube-inner {
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
  width: 100%; }
.video-media-youtube-inner-bio {
    width: 65%; 
    margin-left: auto;
    margin-right: auto;
    }
.media__embed {
  flex-basis: 100%;
  padding-bottom: 1.25rem; }
.video-media-youtube-inner-vi {
  display: block;
  overflow: hidden;
  position: relative;
  height: 0;
  padding: 0; }
.video-media-youtube-inner-vi--ratio {
  padding-bottom: 65%; }
.video-media-youtube-inner-vi iframe {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  width: 100%;
  height: 100%;}


       

/* hero styles */
.hero.is-primary { 
    background-color: rgba(71, 169, 218, 1); 
    background: url("https://wethepeople2019.org/wp-content/uploads/2019/03/thepeople.png") no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover; }
.line-height-add {
   line-height: 1.5; }
.title.is-4 {
    font-weight: normal;
/*    background: rgba(71, 169, 218, 1);*/ }
.title.is-4 span { font-weight: normal !important;}       
      
       
       
/* form section */     

.ngp-form {
    margin: 0 auto;
    max-width: 100% !important;
    }   

.at .at-fieldset { min-width: 75% !important;}

fieldset.ContactInformation { float: left; }
div.FirstName, div.PostalCode, div.EmailAddress { width: 33%; float: left; }

div.at-form-submit { width: 25%; float: left; }    
    
.at input[type="submit"] { width: 100%; font-weight: bold;
        font-size: 1.3em; }
    

    
fieldset.ContactInformation legend.at-legend,
.FooterHtml { display: none;}

header.at-title {display: none; text-align: left !important;}


 
/* rearrange inputs */    
.at-fields {
    display: flex;
    flex-flow: row wrap;
/*    flex-direction: column; */
}
    
    div.FirstName {
        order: 1 !important;
    }
    div.EmailAddress {
        order: 2;
    }
    div.PostalCode { 
        order: 3;
    }
    
/* general styles */      
.blue {
    background-color: #008BD0; 
    padding: 2em;
    }   

.blue .title,
.blue .columns,
.blue .copyright {
    color: #ffffff;
    }
.blue p {
    margin-bottom: 2em;
    }
    
    .blue .copyright p { padding: 3em; }
footer {
    margin-top: 3em;
    padding: 3em 0 !important;
    }      

    figure.biopic { margin: 0 auto; }   

          .privacy-policy { clear: both;}
          .privacy-policy h4 a {color: #ffffff; text-decoration: underline; font-size: 140%;}
          #host-container {  margin-bottom: 10px; } /* height: 84px; */
          .table { display: table; margin: 0 auto; }
          ul#horizontal-list { max-width: 100%; list-style: none; padding: 2em; background-color: #008BD0; line-height: 3;  }
	      ul#horizontal-list li { display: inline; margin-right: 30px; }
          ul#horizontal-list li img { height: 40px; width: auto; }

    h3.bio-name { font-weight: bold !important; }
      
    .hero-body span {font-size: 200%;}     

    section.join h3 { text-align: center !important;}  

    section.sm { padding: 2em; height: 80em; }
    
    section.sm iframe { height: 65em; }
    
    .is-rounded { border: 4px solid #ffffff; } 
    
    h2.title.is-1 {padding: 1em 0;}
    
    
    
    
    
    
@media only screen and (max-width: 768px) {

   .video-media-youtube-inner-bio {
    width: 100%;   
    } 
    
    h3.bio-name { margin-top: 2em; font-weight: bold !important;}
    
    .title.is-1 {
        font-size: 2rem; }
    
}
    
    
    
    @media only screen and (max-width: 970px) {

    .at-fields {        
       flex-direction: column; 
    }

        .at .at-fieldset { min-width: 100% !important;}

fieldset.ContactInformation { float: none; }
div.FirstName, div.PostalCode, div.EmailAddress { width: 100%; float: none; }

div.at-form-submit { width: 100%; float: none; }    
    
.at input[type="submit"] { width: 100%; }


        

    }
    
    
</style>
      
      
  </head>
  <body>

      <!-- Hero Start -->
      
      <section class="hero is-primary">
      <div class="hero-body">
          <div class="container">
                  

<div class="columns">
              
<div class="column has-text-centered">
<h1 class="title is-2 is-invisible" style="display: none;">We The People Membership Summit</h1> <!-- is-invisible -->
<img alt="We The People Logo" src="https://wethepeople2019.org/wp-content/uploads/2019/03/wethepeoplelogo.png" width="300" height="163" ><br> <span>Membership Summit</span>
</div>
</div>    
  
              
              <div class="columns">
                <div class="column">
                    
                    
                     <div class="video-media-youtube-inner-bio">
        <div class="media__embed">
        	<div class="video-media-youtube-inner-vi video-media-youtube-inner-vi--ratio">
                
         
<iframe width="560" height="315" src="https://www.nbcnews.com/news/embedded-video/mmvo1466107971722" scrolling="no" frameborder="0" allowfullscreen></iframe>
      
                  </div>
        </div>
        </div><!-- END .video-media-youtube-inner -->

                    
                    
                    
                    
                    
                    
                </div>          
              </div><!-- END. columns -->
              
              
              
              
          
          
          </div><!-- END .container -->
    </div> <!-- END .hero-body -->
      
      </section>
      
        
      
      
<!--

  <section class="section join">
    <div class="container">
        

        
     
<script type='text/javascript' src='https://d1aqhv4sn5kxtx.cloudfront.net/actiontag/at.js' crossorigin='anonymous'></script>
 <div class="ngp-form"
     data-form-url="https://actions.everyaction.com/v1/Forms/Bt4k_IspGkepaYvlNHHDrg2"
     data-fastaction-endpoint="https://fastaction.ngpvan.com"
     data-inline-errors="true"
     data-fastaction-nologin="true"
     data-databag-endpoint="https://profile.ngpvan.com"
     data-databag="everybody">
</div>
        
        
        <h3 class="title is-4"><strong>Join us live online</strong> <span>as these progressive leaders answer questions directly from people, like you, who are fighting for change.</span></h3> 
        
        
        
        
        
    </div>
  </section>

      
-->
 
      
   <section class="blue" style="border-top: 5px solid #ffffff;">
      <div class="container">

         <h2 class="title is-1">Speakers</h2>
      
       
<div class="columns is-gapless">
  <div class="column is-2">
     <figure class="image is-128x128 biopic">
  <img class="is-rounded" src="https://wethepeople2019.org/wp-content/uploads/2019/03/Cory_Booker.jpg" alt="Senator Corey Booker (D-NJ)">
</figure>
  </div>
  <div class="column">
    <h3 class="title is-4 bio-name">Senator Cory Booker (D-NJ)</h3>
      
      <p class="is-size-5">Senator Booker started his public service in Newark, New Jersey, where he founded a nonprofit organization to provide legal services for low-income families. At age twenty-nine, he was elected to the Newark City Council, and beginning in 2006, he served as Newark’s mayor for more than seven years. Senator Booker is the first African-American elected to the US Senate from New Jersey. In the Senate, he has championed policies to reform the broken criminal justice system, increase wages and ensure hard work is fairly rewarded.</p>

              <hr>
      
  </div> <!-- END .column -->
</div>   <!-- END .columns -->
          
          
              
<div class="columns is-gapless">
  <div class="column is-2">
     <figure class="image is-128x128 biopic">
  <img class="is-rounded" src="https://wethepeople2019.org/wp-content/uploads/2019/03/Julian_Castro.jpg" alt="Julián Castro, former U.S. Secretary of Housing and Urban Development">
</figure>
  </div>
  <div class="column">
    <h3 class="title is-4 bio-name">Julián Castro, former U.S. Secretary of Housing and Urban Development</h3>
      
      <p class="is-size-5">Julián Castro served as the 16th Secretary of the U.S. Department of Housing and Urban Development under President Barack Obama. Before serving in the Cabinet, Secretary Castro was the Mayor of his native San Antonio, Texas — the youngest mayor of a Top 50 American city at the time. You may remember him from his incredible 2012 keynote speech at the Democratic National Convention, where he talked about the American Dream as a relay to be passed from generation to generation. In 2018, Castro founded Opportunity First, an organization created to invest in the next generation of progressive leaders across the country. He announced he was running for President in January.</p>

              <hr>
      
  </div> <!-- END .column -->
</div>   <!-- END .columns -->
          
          


    
<div class="columns is-gapless">
  <div class="column is-2">
     <figure class="image is-128x128 biopic">
  <img class="is-rounded" src="https://wethepeople2019.org/wp-content/uploads/2019/03/Amy_Klobuchar.jpg" alt="Senator Amy Klobuchar (D-MN)">
</figure>
  </div>
  <div class="column">
    <h3 class="title is-4 bio-name">Senator Amy Klobuchar (D-MN)</h3>
      
      <p class="is-size-5">Senator Amy Klobuchar is the first woman elected to the United States Senate in Minnesota’s history. She is the granddaughter of an iron ore miner, the daughter of a newspaperman and an elementary school teacher, and has spent her career in the Senate asking tough questions and working to get things done.</p>
      
      <p class="is-size-5">A recent Vanderbilt University study ranked Senator Klobuchar as the most effective Democratic senator in the last Congress. She’s taken on big Pharma, dark money, voter suppression - and, you may remember - Brett Kavanaugh.</p>

              <hr>
      
  </div> <!-- END .column -->
</div>   <!-- END .columns -->
          
          



    
<div class="columns is-gapless">
  <div class="column is-2">
     <figure class="image is-128x128 biopic">
  <img class="is-rounded" src="https://wethepeople2019.org/wp-content/uploads/2019/03/Beto_ORourke.jpg" alt="Former Representative Beto O’Rourke (D-TX)
">
</figure>
  </div>
  <div class="column">
    <h3 class="title is-4 bio-name">Former Representative Beto O’Rourke (D-TX)</h3>
      
      <p class="is-size-5">Beto O’Rourke took on a 16-year incumbent to run for Congress in 2012, and won. For six years, he represented one of the world’s largest binational communities. He has been a leading voice on immigration and an advocate for the U.S.-Mexico border, including organizing in opposition to family separation. In March of 2017, Beto launched the largest grassroots Senate campaign the Lone Star state had ever seen — and received more votes than any Democrat in Texas history.</p>

              <hr>
      
  </div> <!-- END .column -->
</div>   <!-- END .columns -->
          
          

          
           
<div class="columns is-gapless">
  <div class="column is-2">
     <figure class="image is-128x128 biopic">
  <img class="is-rounded" src="https://wethepeople2019.org/wp-content/uploads/2019/03/Bernie_Sanders.jpg" alt="Senator Bernie Sanders (D-VT)">
</figure>
  </div>
  <div class="column">
    <h3 class="title is-4 bio-name">Senator Bernie Sanders (D-VT)</h3>
      
      <p class="is-size-5">Bernie Sanders is a third-term junior Senator from Vermont, running for president of the United States on a platform of economic, social, racial and environmental justice. Previously, Sanders spent 16 years as Vermont’s sole congressman in the House of Representatives after serving as mayor of Burlington, Vermont, where he fought to improve healthcare for low-income residents and passed one of the nation’s first LGBT anti-discrimination laws. Over the last two decades in Congress, Bernie has consistently led progressives forward on issues ranging from his 1999 fight to keep the Glass-Steagall banking regulations whose repeal contributed to the financial crisis, opposing both wars in Iraq and ending U.S. support for the Saudi-led war in Yemen, and a universal Medicare for All health care system.  In his career as a public servant, Bernie has fought tirelessly for working families, focusing on the shrinking middle class, a $15 minimum wage, labor rights, environmental protection, campaign finance reform, and criminal justice reform, among others.</p>

              <hr>
      
  </div> <!-- END .column -->
</div>   <!-- END .columns -->
          
          


   
      

    
<div class="columns is-gapless">
  <div class="column is-2">
     <figure class="image is-128x128 biopic">
  <img class="is-rounded" src="https://wethepeople2019.org/wp-content/uploads/2019/03/Elizabeth_Warren.jpg" alt="Senator Elizabeth Warren (D-MA)">
</figure>
  </div>
  <div class="column">
    <h3 class="title is-4 bio-name">Senator Elizabeth Warren (D-MA)</h3>
      
      <p class="is-size-5">Senator Elizabeth Warren grew up in Oklahoma, one of four children in a middle class family. She became a teacher, a law professor, and the senior senator from Massachusetts. She’s spent her career studying why America’s middle class has been hollowed out, and taking on the forces that got us here — from big banks and credit card companies to for-profit schools.</p>

              <hr>
      
  </div> <!-- END .column -->
</div>   <!-- END .columns -->
          
          



    
<div class="columns is-gapless">
  <div class="column is-2">
     <figure class="image is-128x128 biopic">
  <img class="is-rounded" src="https://wethepeople2019.org/wp-content/uploads/2019/03/Jay_Inslee.jpg" alt="Governor Jay Inslee (D-WA)">
</figure>
  </div>
  <div class="column">
    <h3 class="title is-4 bio-name">Governor Jay Inslee (D-WA)</h3>
      
      <p class="is-size-5">Washington state Governor Jay Inslee. He’s been governor of the other Washington since 2013, and during his tenure the state has been ranked the best place to work, the #1 economy in the nation and one of the country’s leading states for clean energy. Before becoming governor, he was a member of Congress, where he championed investments to grow America’s clean energy industries. Now he wants to bring his progressive vision to the rest of the country as president. </p>

              <hr>
      
  </div> <!-- END .column -->
</div>   <!-- END .columns -->
          
          




    
<div class="columns is-gapless">
  <div class="column is-2">
     <figure class="image is-128x128 biopic">
  <img class="is-rounded" src="https://wethepeople2019.org/wp-content/uploads/2019/03/Kirsten_Gillibrand.jpg" alt="Senator Kirsten Gillibrand (D-NY)">
</figure>
  </div>
  <div class="column">
    <h3 class="title is-4 bio-name">Senator Kirsten Gillibrand (D-NY)</h3>
      
      <p class="is-size-5">New York’s Senator Kirsten Gillibrand led the repeal of Don't Ask, Don't Tell, spearheaded the passage of the 9/11 Health Bill, and wrote the STOCK Act to ban insider trading by members of Congress. She has been the senate’s leading voice against sexual assault on college campuses and in the military. Before becoming a senator, she held a seat in the House of Representatives — she served as a law clerk and in the Department of Housing and Urban Development during the Clinton Administration. Now, she’s running for president and taking on Trump. </p>

              <hr>
      
  </div> <!-- END .column -->
</div>   <!-- END .columns -->
          
          


         <h2 class="title is-1">Movement Speakers</h2>

                    
          <div class="columns is-gapless">
  <div class="column is-2">
     <figure class="image is-128x128 biopic">
  <img class="is-rounded" src="https://wethepeople2019.org/wp-content/uploads/2019/03/Cristina-Jimenez.png" alt="Cristina Jimenez, United We Dream">
</figure>
  </div>
    
    
  <div class="column">
    <h3 class="title is-4 bio-name">Cristina Jimenez, United We Dream</h3>
      
      <p class="is-size-5">Born in Ecuador, Cristina Jiménez fled poverty with her parents and settled in Queens, NY at the age of 13. As an undocumented person, she and her family experienced abuse by police, wage theft, and fear of deportation. While in college, she realized the transformative power immigrant youth have by telling their stories and developing their own political strategy. In 2008 she cofounded United We Dream, the largest immigrant youth-led organization in the country, and under President Obama she was instrumental in organizing the successful campaign that led to the creation and implementation of DACA. Jiménez holds a master’s degree from the School of Public of Affairs at Baruch College, CUNY and has received several high-profile awards, such as a MacArthur Foundation fellowship in 2017.</p>
      
      
    
      
  </div> <!-- END .column -->
</div>   <!-- END .columns -->
       
      
          
          
                    
          <div class="columns is-gapless">
  <div class="column is-2">
     <figure class="image is-128x128 biopic">
  <img class="is-rounded" src="https://wethepeople2019.org/wp-content/uploads/2019/03/Nancy.jpg" alt="Nancy MacLean, Duke University">
</figure>
  </div>
    
    
  <div class="column">
    <h3 class="title is-4 bio-name">Nancy MacLean, Duke University</h3>
      
      <p class="is-size-5">Nancy MacLean is an American historian, Duke University professor of history and public policy, and award-winning author of 5 books and numerous articles and review essays. Her new book, <em>Democracy in Chains: The Deep History of the Radical Right’s Stealth Plan for America</em>, was described by <em>Publishers Weekly</em> as “a thoroughly researched and gripping narrative… [and] a feat of American intellectual and political history.” <em>Booklist</em> called it “perhaps the best explanation to date of the roots of the political divide that threatens to irrevocably alter American government.” As an award-winning teacher and committed graduate student mentor, MacLean offers courses on twentieth-century America, social movements, and public policy history.
      </p>
        
      
  </div> <!-- END .column -->
</div>   <!-- END .columns -->
       
      
          
          
    
<h2 class="title is-1">Hosted By</h2>
      
       
<div class="columns is-gapless">
  <div class="column is-2">
     <figure class="image is-128x128 biopic">
  <img class="is-rounded" src="https://wethepeople2019.org/wp-content/uploads/2019/03/Dr_Leana_Wen_Jan_2013.jpg" alt="Dr. Leana Wen">
</figure>
  </div>
  <div class="column">
    <h3 class="title is-4 bio-name">Dr. Leana Wen, Planned Parenthood Action Fund</h3>
      
      <p class="is-size-5">Dr. Leana Wen is the President of the Planned Parenthood Federation of America and the Planned Parenthood Action Fund. Before joining Planned Parenthood Action Fund, Dr. Wen served as the Baltimore City Health Commissioner, where she oversaw more than 1,000 employees with an annual budget of $130 million; two clinics that provide more than 18,000 patients with reproductive health services; and mental health programs in 180 Baltimore schools. A board-certified emergency physician, Dr. Wen was a Rhodes Scholar, Clinical Fellow at Harvard, consultant with the World Health Organization, and professor at George Washington University. She has published over 100 scientific articles and is the author of the book <em>When Doctors Don’t Listen</em>. In 2016, Dr. Wen was honored to be the recipient of the American Public Health Association’s highest award for local public health work. In 2018, <em>Time Magazine</em> named her one of the 50 most influential people in healthcare.
      </p>

              <hr>
      
  </div> <!-- END .column -->
</div>   <!-- END .columns -->
          
          
          
    
          
          
          
          
          <div class="columns is-gapless">
  <div class="column is-2">
     <figure class="image is-128x128 biopic">
  <img class="is-rounded" src="https://wethepeople2019.org/wp-content/uploads/2019/03/JenniferEpps-Addison-JD.jpg" alt="Jennifer Epps-Addison, JD">
</figure>
  </div>
  <div class="column">
    <h3 class="title is-4 bio-name">Jennifer Epps-Addison, JD, Center for Popular Democracy Action</h3>
      <p class="is-size-5">Jennifer Epps-Addison, JD, is the Network President and Co-Executive Director of the Center for Popular Democracy Action, a nationwide racial and social justice advocacy organization. She boasts over 15 years of organizing experience and a history of activism with accomplishments including leading the campaign for paid sick days and fighting for in-state tuition for undocumented immigrants. Epps-Addison leads CPD Action’s racial justice campaigns and works with the organization’s growing network of local affiliates. She holds a BA in Political Science and Women’s Studies and a JD from the University of Wisconsin.
      </p>  
            <hr>
  </div> <!-- END .column -->          
</div>   <!-- END .columns -->
          
          
          
    
          
          
          
          
          <div class="columns is-gapless">
  <div class="column is-2">
     <figure class="image is-128x128 biopic">
  <img class="is-rounded" src="https://wethepeople2019.org/wp-content/uploads/2019/03/Michael-Brune.jpg" alt="Michael Brune, Sierra Club">
</figure>
  </div>
    
    
  <div class="column">
    <h3 class="title is-4 bio-name">Michael Brune, Sierra Club</h3>
      
      <p class="is-size-5">Michael Brune is the executive director of the Sierra Club and the author of Coming Clean: Breaking America’s Addiction to Oil and Coal (2010). During Brune’s nearly nine years as the Sierra Club’s executive director, the organization has grown to more than three and a half million supporters, and its Beyond Coal campaign has been recognized as one of the most effective in environmental history. Brune came to the Sierra Club from the Rainforest Action Network, where he served seven years as executive director. Under Brune’s leadership, Rainforest Action Network won more than a dozen key environmental commitments from America’s largest corporations, including Bank of America, Boise, Citi, Goldman Sachs, Home Depot, Kinko’s, and Lowe’s. He and his wife, Mary, attribute their ongoing passion for environmental activism in part to concern that their children inherit a healthy world.


      </p>
      
    <hr> 
        
      
  </div> <!-- END .column -->
</div>   <!-- END .columns -->
       

          
          
          
          
          
              <div class="columns is-gapless">
  <div class="column is-2">
     <figure class="image is-128x128 biopic">
  <img class="is-rounded" src="https://wethepeople2019.org/wp-content/uploads/2019/03/MKH-2016-Convention.png" alt="Mary Kay Henry, SEIU">
</figure>
  </div>
    
    
  <div class="column">
    <h3 class="title is-4 bio-name">Mary Kay Henry, Service Employees International Union</h3>
      
      <p class="is-size-5">Mary Kay Henry is International President of the 2 million-member Service Employees International Union (SEIU), and her leadership is rooted in a deep-seated belief that when individuals join together they can make the impossible possible. Under her leadership, SEIU has won major victories to improve working families’ lives by strengthening and uniting healthcare, property services, and public sector workers with other working people across the United States, Canada and Puerto Rico.
      </p>
       <hr> 
  </div> <!-- END .column -->
</div>   <!-- END .columns -->
       
            
          
          
         
        
          
          
          
          
          
                    <div class="columns is-gapless">
  <div class="column is-2">
     <figure class="image is-128x128 biopic">
  <img class="is-rounded" src="https://wethepeople2019.org/wp-content/uploads/2019/03/7956CB12-1F5C-4AA8-B7A4-5D1C0DD9C9A2.jpg" alt=" Hector Figueroa, SEIU 32BJ">
</figure>
  </div>
    
    
  <div class="column">
    <h3 class="title is-4 bio-name">Hector Figueroa, President of 32BJ Service Employees International Union</h3>
      
      <p class="is-size-5">Héctor J. Figueroa was elected President of 32BJ Service Employees International Union, the largest property services union in the country, in 2012.  32BJ represents more than 163,000 property service workers – window cleaners, airport workers, superintendents, doormen, maintenance workers, cleaners, porters and security officers – in New York, New Jersey, Connecticut, Massachusetts, Rhode Island, New Hampshire, Florida, Pennsylvania, Delaware, Virginia, Maryland and Washington, D.C. Under Héctor’s presidency, 32BJ has grown by nearly 50,000 members – 18,000 through the merger with Local 615 and another 30,000 through organizing campaigns – and has passed dozens of local and state policies protecting and lifting working families up and down the East Coast.</p>
          
         <hr>
      
  </div> <!-- END .column -->
</div>   <!-- END .columns -->
       
      
          
          
          
          
         
          
          
          
          
                    <div class="columns is-gapless">
  <div class="column is-2">
     <figure class="image is-128x128 biopic">
  <img class="is-rounded" src="https://wethepeople2019.org/wp-content/uploads/2019/03/CWA-President-Chris-Shelton.jpg" alt="Chris Shelton, CWA">
</figure>
  </div>
  <div class="column">
    <h3 class="title is-4 bio-name">Chris Shelton, Communications Workers of America</h3>
      
      <p class="is-size-5">Christopher Shelton was elected president of the Communications Workers of America by acclamation by delegates to the union’s 75th convention on June 8, 2015. Prior to his election as president, Shelton served as vice president of CWA District 1, representing 160,000 members in more than 300 CWA locals in New Jersey, New York and New England. He served as the Verizon Regional Bargaining Chair in 2000 and 2003, and overall Chair of Verizon bargaining for CWA District 1, District 2-13, IBEW New Jersey and New England in 2008 and 2011. He also chaired negotiations in New Jersey for 40,000 State Workers in 2008 and 2011. Shelton started his union career when he went to work for New York Telephone in 1968 as an outside technician. He was elected a CWA Local 1101 shop steward in 1968 and served Local 1101 in various positions until December 1988 when he joined the CWA national staff.
      </p>  
  </div> <!-- END .column -->
</div>   <!-- END .columns -->
       
      
          
          
          
          
          
       
       
       
       </div> <!-- END .container --> 
       </section>
      

        
       
      <section class="sm">
       <div class="container">
           
         <h2 class="title is-1">Follow #WeThePeople19 to join the conversation</h2>  
           
           <iframe src='https://keyhole.co/hashtag-tracking/media-wall/Q0xGw2/WeThePeople2019?embed=1&' frameborder='0' height='100%' width='100%'></iframe>
           
           
         
       </div>
      </section>
      
      
      
     

      <footer class="blue">
   <div class="container">
            
               <div class="columns">
                <div class="column">
              
     <div id="host-container">
		<div class="table">
			<ul id="horizontal-list">
                <li><img src="https://wethepeople2019.org/wp-content/uploads/2019/03/CPD-Action-log.png" alt="CPD Action" /></li>
				<li><img src="https://wethepeople2019.org/wp-content/uploads/2019/03/CWA-Logo.png" alt="Communications Workers of America" /></li>
				<li><img src="https://wethepeople2019.org/wp-content/uploads/2019/03/PP-Logo-1.png" alt="Planned Parenthood Action Fund" /></li>
                <li><img src="https://wethepeople2019.org/wp-content/uploads/2019/03/SEIU-logo.png" alt="SEIU logo" /></li>
                <li><img src="https://wethepeople2019.org/wp-content/uploads/2019/03/seiu32bj-white-logo.png" alt="SEIU 32BJ Logo" /> </li>
				<li><img src="https://wethepeople2019.org/wp-content/uploads/2019/03/Seirra-Club-Logo.png" alt="Sierra Club" /></li>
                
                
			</ul>
		</div>
	</div><!-- END .host-container -->
              
                   </div> <!-- END .column -->
              </div><!-- END .columns -->
              
            
              
              <div class="columns">
                <div class="column">
          <div class="privacy-policy has-text-centered copyright">
              <p>&copy; 2019 CPD Action, Communications Workers of America, Planned Parenthood Action Fund, SEIU, SEIU 32BJ, Sierra Club</p>

              
              <h4><a href="https://wethepeople2019.org/privacy-policy/">Privacy Policy</a></h4>
                
              
          </div><!-- END privacy-policy -->
                </div>
            </div> <!-- END .columns -->  
            

          
          
          
          </div> <!-- END .container -->
          

      
          
          
      
      </footer>
      
      
  </body>
</html>