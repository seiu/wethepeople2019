<?php
/*
Template Name: Custom Home Page
Description: Hard coded home page
*/
?>
 

<!DOCTYPE html>
<html>
  <head>
      <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-136671218-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-136671218-1');
</script>

      
      
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>We The People</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css">
<!--    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>-->
      
      <link rel="icon" href="https://wethepeople2019.org/wp-content/uploads/2019/03/cropped-wethepeoplelogo-icon-32x32.png" sizes="32x32" />
<link rel="icon" href="https://wethepeople2019.org/wp-content/uploads/2019/03/cropped-wethepeoplelogo-icon-192x192.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="https://wethepeople2019.org/wp-content/uploads/2019/03/cropped-wethepeoplelogo-icon-180x180.png" />
<meta name="msapplication-TileImage" content="https://wethepeople2019.org/wp-content/uploads/2019/03/cropped-wethepeoplelogo-icon-270x270.png" />
      
      

<meta property="og:url"                content="https://wethepeople2019.org/" />
<meta property="og:type"               content="website" />
<meta property="og:title"              content="We The People Membership Summit" />
<meta property="og:description"        content="April 1st Americans across the nation will have the opportunity to hear from some of the most important voices on the political and public policy stage" />
<meta property="og:image"              content="http://wethepeople2019.org/wp-content/uploads/2019/03/We-The-People.png" />
      
      
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@SEIU" />
      
      
      
<style>
    
.ht-header {background-color: #008bd0 !important; }
    
    
/* youtube embed responsive */       
.video-media-youtube {
  width: 100%;
  max-width: 1400px;
  margin: 0 auto; }
.video-media-youtube-inner {
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
  width: 100%; }
.video-media-youtube-inner-bio {
    width: 50%;   
    }
.media__embed {
  flex-basis: 100%;
  padding-bottom: 1.25rem; }
.video-media-youtube-inner-vi {
  display: block;
  overflow: hidden;
  position: relative;
  height: 0;
  padding: 0; }
.video-media-youtube-inner-vi--ratio {
  padding-bottom: 75%; }
.video-media-youtube-inner-vi iframe {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  width: 100%;
  height: 100%;}


       

/* hero styles */
.hero.is-primary { 
    background-color: rgba(71, 169, 218, 1); 
    background: url("http://wethepeople2019.org/wp-content/uploads/2019/03/thepeople.png") no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover; }
.line-height-add {
   line-height: 1.5; }
.title.is-4 {
    font-weight: normal;
/*    background: rgba(71, 169, 218, 1);*/ }
.title.is-4 span { font-weight: normal !important;}       
      
       
       
/* form section */     

.ngp-form {
    margin: 0 auto;
    max-width: 100% !important;
    }   

.at .at-fieldset { min-width: 75% !important;}

fieldset.ContactInformation { float: left; }
div.FirstName, div.PostalCode, div.EmailAddress { width: 33%; float: left; }

div.at-form-submit { width: 25%; float: left; }    
    
.at input[type="submit"] { width: 100%; font-weight: bold;
        font-size: 1.3em; }
    

    
fieldset.ContactInformation legend.at-legend,
.FooterHtml { display: none;}

header.at-title {display: none; text-align: left !important;}


 
/* rearrange inputs */    
.at-fields {
    display: flex;
    flex-flow: row wrap;
/*    flex-direction: column; */
}
    
    div.FirstName {
        order: 1 !important;
    }
    div.EmailAddress {
        order: 2;
    }
    div.PostalCode { 
        order: 3;
    }
    
/* general styles */      
.blue {
    background-color: #008BD0; 
    padding: 2em;
    }   

.blue .title,
.blue .columns,
.blue .copyright {
    color: #ffffff;
    }
.blue p {
    margin-bottom: 2em;
    }
    
    .blue .copyright p { padding: 3em; }
footer {
    margin-top: 3em;
    padding: 3em 0 !important;
    }      

    figure.biopic { margin: 0 auto; }   

          .privacy-policy { clear: both;}
          .privacy-policy h4 a {color: #ffffff; text-decoration: underline; font-size: 140%;}
          #host-container {  margin-bottom: 10px; } /* height: 84px; */
          .table { display: table; margin: 0 auto; }
          ul#horizontal-list { max-width: 100%; list-style: none; padding: 2em; background-color: #008BD0; line-height: 3;  }
	      ul#horizontal-list li { display: inline; margin-right: 30px; }
          ul#horizontal-list li img { height: 40px; width: auto; }

    h3.bio-name { font-weight: bold !important; }
      
    .hero-body span {font-size: 200%;}     

    section.join h3 { text-align: center !important;}  

    
@media only screen and (max-width: 768px) {

   .video-media-youtube-inner-bio {
    width: 100%;   
    } 
h3.bio-name { margin-top: 2em; font-weight: bold !important;}
    
}
    
    
    
    @media only screen and (max-width: 970px) {

    .at-fields {        
       flex-direction: column; 
    }

        .at .at-fieldset { min-width: 100% !important;}

fieldset.ContactInformation { float: none; }
div.FirstName, div.PostalCode, div.EmailAddress { width: 100%; float: none; }

div.at-form-submit { width: 100%; float: none; }    
    
.at input[type="submit"] { width: 100%; }


        

    }
    
    
</style>
      
      
  </head>
  <body>

      <!-- Hero Start -->
      
      <section class="hero is-primary">
      <div class="hero-body">
          <div class="container">
                  

<div class="columns">
              
<div class="column has-text-centered">
<h1 class="title is-2 is-invisible" style="display: none;">We The People Membership Summit</h1> <!-- is-invisible -->
<img alt="We The People Logo" src="https://wethepeople2019.org/wp-content/uploads/2019/03/wethepeoplelogo.png" width="300" height="163" ><br> <span>Membership Summit</span>
</div>
</div>    
  
              
              <div class="columns">
                <div class="column">
                    <div class="intro-text">
              
              <h2 class="title is-4 line-height-add"><strong>On April 1st</strong> Americans across the nation will have the opportunity to hear from some of the most important voices on the political and public policy stage to lay out their plans for how we can continue to build on gains made through our fight for racial justice, economic justice, reproductive rights and the environment, and build towards transformational democracy reform.</h2> 
                  
              </div>
                </div>          
              </div><!-- END. columns -->
              
              
              
              
          
          
          </div><!-- END .container -->
    </div> <!-- END .hero-body -->
      
      </section>
      
      
      
  <section class="section join">
    <div class="container">
        

        
     
<script type='text/javascript' src='https://d1aqhv4sn5kxtx.cloudfront.net/actiontag/at.js' crossorigin='anonymous'></script>
 <div class="ngp-form"
     data-form-url="https://actions.everyaction.com/v1/Forms/Bt4k_IspGkepaYvlNHHDrg2"
     data-fastaction-endpoint="https://fastaction.ngpvan.com"
     data-inline-errors="true"
     data-fastaction-nologin="true"
     data-databag-endpoint="https://profile.ngpvan.com"
     data-databag="everybody">
</div>
        
        
        <h3 class="title is-4"><strong>Join us live online</strong> <span>as these progressive leaders answer questions directly from people, like you, who are fighting for change.</span></h3> 
        
        
        
        
        
    </div>
  </section>
      
   <section class="blue">
      <div class="container">

         <h2 class="title is-1">Speakers</h2>
      
       
<div class="columns is-gapless">
  <div class="column is-2">
     <figure class="image is-128x128 biopic">
  <img class="is-rounded" src="http://wethepeople2019.org/wp-content/uploads/2019/03/Dr_Leana_Wen_Jan_2013.jpg" alt="Dr. Leana Wen">
</figure>
  </div>
  <div class="column">
    <h3 class="title is-4 bio-name">Dr. Leana Wen, Planned Parenthood Action Fund</h3>
      
      <p class="is-size-5">Dr. Leana Wen is the President of the Planned Parenthood Federation of America and the Planned Parenthood Action Fund. Before joining Planned Parenthood Action Fund, Dr. Wen served as the Baltimore City Health Commissioner, where she oversaw more than 1,000 employees with an annual budget of $130 million; two clinics that provide more than 18,000 patients with reproductive health services; and mental health programs in 180 Baltimore schools. A board-certified emergency physician, Dr. Wen was a Rhodes Scholar, Clinical Fellow at Harvard, consultant with the World Health Organization, and professor at George Washington University. She has published over 100 scientific articles and is the author of the book <em>When Doctors Don’t Listen</em>. In 2016, Dr. Wen was honored to be the recipient of the American Public Health Association’s highest award for local public health work. In 2018, <em>Time Magazine</em> named her one of the 50 most influential people in healthcare.
      </p>

              <hr>
      
  </div> <!-- END .column -->
</div>   <!-- END .columns -->
          
          
          
    
          
          
          
          
          <div class="columns is-gapless">
  <div class="column is-2">
     <figure class="image is-128x128 biopic">
  <img class="is-rounded" src="http://wethepeople2019.org/wp-content/uploads/2019/03/JenniferEpps-Addison-JD.jpg" alt="Jennifer Epps-Addison, JD">
</figure>
  </div>
  <div class="column">
    <h3 class="title is-4 bio-name">Jennifer Epps-Addison, JD, Center for Popular Democracy Action</h3>
      <p class="is-size-5">Jennifer Epps-Addison, JD, is the Network President and Co-Executive Director of the Center for Popular Democracy Action, a nationwide racial and social justice advocacy organization. She boasts over 15 years of organizing experience and a history of activism with accomplishments including leading the campaign for paid sick days and fighting for in-state tuition for undocumented immigrants. Epps-Addison leads CPD Action’s racial justice campaigns and works with the organization’s growing network of local affiliates. She holds a BA in Political Science and Women’s Studies and a JD from the University of Wisconsin.
      </p>  
            <hr>
  </div> <!-- END .column -->          
</div>   <!-- END .columns -->
          
          
          
    
          
          
          
          
          <div class="columns is-gapless">
  <div class="column is-2">
     <figure class="image is-128x128 biopic">
  <img class="is-rounded" src="http://wethepeople2019.org/wp-content/uploads/2019/03/Michael-Brune.jpg" alt="Michael Brune, Sierra Club">
</figure>
  </div>
    
    
  <div class="column">
    <h3 class="title is-4 bio-name">Michael Brune, Sierra Club</h3>
      
      <p class="is-size-5">Michael Brune is the executive director of the Sierra Club and the author of Coming Clean: Breaking America’s Addiction to Oil and Coal (2010). During Brune’s nearly nine years as the Sierra Club’s executive director, the organization has grown to more than three and a half million supporters, and its Beyond Coal campaign has been recognized as one of the most effective in environmental history. Brune came to the Sierra Club from the Rainforest Action Network, where he served seven years as executive director. Under Brune’s leadership, Rainforest Action Network won more than a dozen key environmental commitments from America’s largest corporations, including Bank of America, Boise, Citi, Goldman Sachs, Home Depot, Kinko’s, and Lowe’s. He and his wife, Mary, attribute their ongoing passion for environmental activism in part to concern that their children inherit a healthy world.


      </p>
      
    <hr> 
        
      
  </div> <!-- END .column -->
</div>   <!-- END .columns -->
       

          
          
          
          
          
              <div class="columns is-gapless">
  <div class="column is-2">
     <figure class="image is-128x128 biopic">
  <img class="is-rounded" src="http://wethepeople2019.org/wp-content/uploads/2019/03/MKH-2016-Convention.png" alt="Mary Kay Henry, SEIU">
</figure>
  </div>
    
    
  <div class="column">
    <h3 class="title is-4 bio-name">Mary Kay Henry, Service Employees International Union</h3>
      
      <p class="is-size-5">Mary Kay Henry is International President of the 2 million-member Service Employees International Union (SEIU), and her leadership is rooted in a deep-seated belief that when individuals join together they can make the impossible possible. Under her leadership, SEIU has won major victories to improve working families’ lives by strengthening and uniting healthcare, property services, and public sector workers with other working people across the United States, Canada and Puerto Rico.
      </p>
       <hr> 
  </div> <!-- END .column -->
</div>   <!-- END .columns -->
       
            
          
          
         
        
          
          
          
          
          
                    <div class="columns is-gapless">
  <div class="column is-2">
     <figure class="image is-128x128 biopic">
  <img class="is-rounded" src="http://wethepeople2019.org/wp-content/uploads/2019/03/7956CB12-1F5C-4AA8-B7A4-5D1C0DD9C9A2.jpg" alt=" Hector Figueroa, SEIU 32BJ">
</figure>
  </div>
    
    
  <div class="column">
    <h3 class="title is-4 bio-name">Hector Figueroa, President of 32BJ Service Employees International Union</h3>
      
      <p class="is-size-5">Héctor J. Figueroa was elected President of 32BJ Service Employees International Union, the largest property services union in the country, in 2012.  32BJ represents more than 163,000 property service workers – window cleaners, airport workers, superintendents, doormen, maintenance workers, cleaners, porters and security officers – in New York, New Jersey, Connecticut, Massachusetts, Rhode Island, New Hampshire, Florida, Pennsylvania, Delaware, Virginia, Maryland and Washington, D.C. Under Héctor’s presidency, 32BJ has grown by nearly 50,000 members – 18,000 through the merger with Local 615 and another 30,000 through organizing campaigns – and has passed dozens of local and state policies protecting and lifting working families up and down the East Coast.</p>
          
         <hr>
      
  </div> <!-- END .column -->
</div>   <!-- END .columns -->
       
      
          
          
          
          
         
          
          
          
          
                    <div class="columns is-gapless">
  <div class="column is-2">
     <figure class="image is-128x128 biopic">
  <img class="is-rounded" src="http://wethepeople2019.org/wp-content/uploads/2019/03/CWA-President-Chris-Shelton.jpg" alt="Chris Shelton, CWA">
</figure>
  </div>
  <div class="column">
    <h3 class="title is-4 bio-name">Chris Shelton, Communications Workers of America</h3>
      
      <p class="is-size-5">Christopher Shelton was elected president of the Communications Workers of America by acclamation by delegates to the union’s 75th convention on June 8, 2015. Prior to his election as president, Shelton served as vice president of CWA District 1, representing 160,000 members in more than 300 CWA locals in New Jersey, New York and New England. He served as the Verizon Regional Bargaining Chair in 2000 and 2003, and overall Chair of Verizon bargaining for CWA District 1, District 2-13, IBEW New Jersey and New England in 2008 and 2011. He also chaired negotiations in New Jersey for 40,000 State Workers in 2008 and 2011. Shelton started his union career when he went to work for New York Telephone in 1968 as an outside technician. He was elected a CWA Local 1101 shop steward in 1968 and served Local 1101 in various positions until December 1988 when he joined the CWA national staff.
      </p>  
  </div> <!-- END .column -->
</div>   <!-- END .columns -->
       
      
          
          
          
          
          

          
          
          
          
          
          
          
       
       
       
       </div> <!-- END .container --> 
       </section>
      


      <footer class="blue">
   <div class="container">
            
               <div class="columns">
                <div class="column">
              
     <div id="host-container">
		<div class="table">
			<ul id="horizontal-list">
                <li><img src="https://wethepeople2019.org/wp-content/uploads/2019/03/CPD-Action-log.png" alt="CPD Action" /></li>
				<li><img src="https://wethepeople2019.org/wp-content/uploads/2019/03/CWA-Logo.png" alt="Communications Workers of America" /></li>
				<li><img src="http://wethepeople2019.org/wp-content/uploads/2019/03/PP-Logo-1.png" alt="Planned Parenthood Action Fund" /></li>
                <li><img src="https://wethepeople2019.org/wp-content/uploads/2019/03/SEIU-logo.png" alt="SEIU logo" /></li>
                <li><img src="http://wethepeople2019.org/wp-content/uploads/2019/03/seiu32bj-white-logo.png" alt="SEIU 32BJ Logo" /> </li>
				<li><img src="https://wethepeople2019.org/wp-content/uploads/2019/03/Seirra-Club-Logo.png" alt="Sierra Club" /></li>
                
                
			</ul>
		</div>
	</div><!-- END .host-container -->
              
                   </div> <!-- END .column -->
              </div><!-- END .columns -->
              
            
              
              <div class="columns">
                <div class="column">
          <div class="privacy-policy has-text-centered copyright">
              <p>&copy; 2019 CPD Action, Communications Workers of America, Planned Parenthood Action Fund, SEIU, SEIU 32BJ, Sierra Club</p>

              
              <h4><a href="https://wethepeople2019.org/privacy-policy/">Privacy Policy</a></h4>
                
              
          </div><!-- END privacy-policy -->
                </div>
            </div> <!-- END .columns -->  
            

          
          
          
          </div> <!-- END .container -->
          

      
          
          
      
      </footer>
      
      
  </body>
</html>