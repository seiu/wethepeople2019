<?php

/**
 * total functions and definitions
 *
 * @package total
 */
if (!function_exists('total_plus_setup')) :

    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function total_plus_setup() {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on total, use a find and replace
         * to change 'total-plus' to the name of your theme in all the template files
         */
        load_theme_textdomain('total-plus', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
         */
        add_theme_support('post-thumbnails');
        add_image_size('total-840x420', 840, 420, true);
        add_image_size('total-600x600', 600, 600, true);
        add_image_size('total-400x400', 400, 400, true);
        add_image_size('total-400x280', 400, 280, true);
        add_image_size('total-350x420', 350, 420, true);
        add_image_size('total-100x100', 100, 100, true);

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'primary' => esc_html__('Primary Menu', 'total-plus'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('total_plus_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));

        add_theme_support('custom-logo', array(
            'height' => 62,
            'width' => 300,
            'flex-height' => true,
            'flex-width' => true,
            'header-text' => array('.ht-site-title', '.ht-site-description'),
        ));

        add_theme_support('woocommerce');
        add_theme_support('wc-product-gallery-zoom');
        add_theme_support('wc-product-gallery-lightbox');
        add_theme_support('wc-product-gallery-slider');

        /*
         * This theme styles the visual editor to resemble the theme style,
         * specifically font, colors, icons, and column width.
         */
        add_editor_style(array('css/editor-style.css', total_plus_fonts_url()));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        // Add support for Block Styles.
        add_theme_support('wp-block-styles');

        // Add support for full and wide align images.
        add_theme_support('align-wide');

        // Add support for editor styles.
        add_theme_support('editor-styles');

        // Add support for responsive embedded content.
        add_theme_support('responsive-embeds');
    }

endif; // total_plus_setup
add_action('after_setup_theme', 'total_plus_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function total_plus_content_width() {
    $GLOBALS['content_width'] = apply_filters('total_plus_content_width', 640);
}

add_action('after_setup_theme', 'total_plus_content_width', 0);

/**
 * Enables the Excerpt meta box in Page edit screen.
 */
function total_plus_add_excerpt_support_for_pages() {
    add_post_type_support('page', 'excerpt');
}

add_action('init', 'total_plus_add_excerpt_support_for_pages');

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function total_plus_widgets_init() {
    register_sidebar(array(
        'name' => esc_html__('Right Sidebar', 'total-plus'),
        'id' => 'total-right-sidebar',
        'description' => __('Add widgets here to appear in your sidebar.', 'total-plus'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Left Sidebar', 'total-plus'),
        'id' => 'total-left-sidebar',
        'description' => __('Add widgets here to appear in your sidebar.', 'total-plus'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>',
    ));

    if (total_plus_is_woocommerce_activated()) {
        register_sidebar(array(
            'name' => esc_html__('Shop Right Sidebar', 'total-plus'),
            'id' => 'total-shop-right-sidebar',
            'description' => __('Add widgets here to appear in your sidebar of shop page.', 'total-plus'),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h4 class="widget-title">',
            'after_title' => '</h4>',
        ));

        register_sidebar(array(
            'name' => esc_html__('Shop Left Sidebar', 'total-plus'),
            'id' => 'total-shop-left-sidebar',
            'description' => __('Add widgets here to appear in your sidebar of shop page.', 'total-plus'),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h4 class="widget-title">',
            'after_title' => '</h4>',
        ));
    }

    register_sidebar(array(
        'name' => esc_html__('Header Widget', 'total-plus'),
        'id' => 'total-header-widget',
        'description' => __('Add widgets in the Header. Works with Header 4 and Header 6 Only', 'total-plus'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Top Footer', 'total-plus'),
        'id' => 'total-top-footer',
        'description' => __('Add widgets here to appear in your Footer.', 'total-plus'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Footer One', 'total-plus'),
        'id' => 'total-footer1',
        'description' => __('Add widgets here to appear in your Footer.', 'total-plus'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Footer Two', 'total-plus'),
        'id' => 'total-footer2',
        'description' => __('Add widgets here to appear in your Footer.', 'total-plus'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Footer Three', 'total-plus'),
        'id' => 'total-footer3',
        'description' => __('Add widgets here to appear in your Footer.', 'total-plus'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Footer Four', 'total-plus'),
        'id' => 'total-footer4',
        'description' => __('Add widgets here to appear in your Footer.', 'total-plus'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>',
    ));
}

add_action('widgets_init', 'total_plus_widgets_init');

if (!function_exists('total_plus_fonts_url')) :

    /**
     * Register Google fonts for Total Plus.
     *
     * @since Total Plus 1.0
     *
     * @return string Google fonts URL for the theme.
     */
    function total_plus_fonts_url() {
        $fonts_url = '';
        $fonts = $standard_font_family = array();
        $subsets = 'latin,latin-ext';
        $variants_array = $font_array = $google_fonts = array();
        $total_plus_standard_font = total_plus_standard_font_array();
        $customizer_fonts = total_plus_get_customizer_fonts();
        $google_font_list = total_plus_google_font_array();

        /*
         * Translators: If there are characters in your language that are not supported
         * by Pontano Sans, translate this to 'off'. Do not translate into your own language.
         */
        if ('off' !== _x('on', 'Pontano Sans font: on or off', 'total-plus')) {
            $font_family_array[] = 'Pontano Sans';
        }

        /*
         * Translators: If there are characters in your language that are not supported
         * by Oswald, translate this to 'off'. Do not translate into your own language.
         */
        if ('off' !== _x('on', 'Oswald font: on or off', 'total-plus')) {
            $font_family_array[] = 'Oswald';
        }

        foreach ($total_plus_standard_font as $key => $value) {
            $standard_font_family[] = $value['family'];
        }

        foreach ($customizer_fonts as $key => $value) {
            $font_family_array[] = get_theme_mod($key . '_font_family', $value['font_family']);
        }

        $font_family_array = array_unique($font_family_array);
        $font_family_array = array_diff($font_family_array, $standard_font_family);

        foreach ($font_family_array as $font_family) {
            $font_array = total_plus_search_key($google_font_list, 'family', $font_family);
            $variants_array = $font_array['0']['variants'];
            $variants_keys = array_keys($variants_array);
            $variants = implode(',', $variants_keys);

            $fonts[] = $font_family . ':' . str_replace('italic', 'i', $variants);
        }
        /*
         * Translators: To add an additional character subset specific to your language,
         * translate this to 'greek', 'cyrillic', 'devanagari' or 'vietnamese'. Do not translate into your own language.
         */
        $subset = _x('no-subset', 'Add new subset (greek, cyrillic, devanagari, vietnamese)', 'total-plus');

        if ('cyrillic' == $subset) {
            $subsets .= ',cyrillic,cyrillic-ext';
        } elseif ('greek' == $subset) {
            $subsets .= ',greek,greek-ext';
        } elseif ('devanagari' == $subset) {
            $subsets .= ',devanagari';
        } elseif ('vietnamese' == $subset) {
            $subsets .= ',vietnamese';
        }

        if ($fonts) {
            $fonts_url = add_query_arg(array(
                'family' => urlencode(implode('|', $fonts)),
                'subset' => urlencode($subsets),
                    ), '//fonts.googleapis.com/css');
        }

        return $fonts_url;
    }

endif;

/**
 * Enqueue scripts and styles.
 */
function total_plus_scripts() {
    $google_map_api = of_get_option('api_key', '');
    $key = '';
    if ($google_map_api) {
        $key = 'key=' . $google_map_api . '&';
        wp_enqueue_script('ht-google-map', '//maps.googleapis.com/maps/api/js?&' . $key . 'sensor=false', array(), '1.00', false);
    }
    wp_enqueue_script('smoothscroll', get_template_directory_uri() . '/js/SmoothScroll.js', array(), '1.00', false);
    wp_enqueue_script('jquery-nav', get_template_directory_uri() . '/js/jquery.nav.js', array('jquery'), '1.00', true);
    wp_enqueue_script('owl-carousel', get_template_directory_uri() . '/js/owl.carousel.js', array('jquery'), '1.3.3', true);
    wp_enqueue_script('owl-filter', get_template_directory_uri() . '/js/jquery.owl-filter.js', array('jquery'), '1.00', true);
    wp_enqueue_script('isotope-pkgd', get_template_directory_uri() . '/js/isotope.pkgd.js', array('jquery', 'imagesloaded'), '1.00', true);
    wp_enqueue_script('lightgallery', get_template_directory_uri() . '/js/lightgallery.js', array('jquery'), '1.00', true);
    wp_enqueue_script('hoverintent', get_template_directory_uri() . '/js/hoverintent.js', array(), '1.00', true);
    wp_enqueue_script('superfish', get_template_directory_uri() . '/js/superfish.js', array('jquery'), '1.00', true);
    wp_enqueue_script('jquery-stellar', get_template_directory_uri() . '/js/jquery.stellar.js', array('imagesloaded'), '1.00', false);
    wp_enqueue_script('wow', get_template_directory_uri() . '/js/wow.js', array('jquery'), '1.00', true);
    wp_enqueue_script('odometer', get_template_directory_uri() . '/js/odometer.js', array('jquery'), '1.00', true);
    wp_enqueue_script('waypoint', get_template_directory_uri() . '/js/waypoint.js', array('jquery'), '1.00', true);
    wp_enqueue_script('YTPlayer', get_template_directory_uri() . '/js/jquery.mb.YTPlayer.min.js', array('jquery'), '1.00', true);
    wp_enqueue_script('espy', get_template_directory_uri() . '/js/jquery.espy.min.js', array('jquery'), '1.00', true);
    wp_enqueue_script('motio', get_template_directory_uri() . '/js/motio.min.js', array('jquery'), '1.00', true);
    wp_enqueue_script('slick', get_template_directory_uri() . '/js/slick.js', array('jquery'), '1.00', true);
    wp_enqueue_script('flipster', get_template_directory_uri() . '/js/jquery.flipster.min.js', array('jquery'), '1.00', true);
    wp_enqueue_script('nicescroll', get_template_directory_uri() . '/js/jquery.nicescroll.min.js', array('jquery'), '1.00', true);
    wp_enqueue_script('jquery-mcustomscrollbar', get_template_directory_uri() . '/js/jquery.mCustomScrollbar.js', array('jquery'), '1.00', true);
    wp_enqueue_script('jquery-accordion', get_template_directory_uri() . '/js/jquery.accordion.js', array('jquery'), '1.00', true);
    wp_enqueue_script('instafeed', get_template_directory_uri() . '/js/instafeed.min.js', array(), '1.00', false);
    wp_enqueue_script('photostream', get_template_directory_uri() . '/js/jquery.photostream.min.js', array('jquery'), '1.00', true);
    wp_enqueue_script('justifiedGallery', get_template_directory_uri() . '/js/jquery.justifiedGallery.min.js', array('jquery'), '1.00', true);
    wp_enqueue_script('countdown', get_template_directory_uri() . '/js/jquery.countdown.js', array('jquery'), '1.00', true);
    wp_enqueue_script('slimmenu', get_template_directory_uri() . '/js/jquery.slimmenu.js', array('jquery'), '1.00', true);
    wp_enqueue_script('total-megamenu', get_template_directory_uri() . '/inc/walker/assets/megaMenu.js', array('jquery'), '1.00', true);
    wp_enqueue_script('headroom', get_template_directory_uri() . '/js/headroom.js', array('jquery'), '1.00', true);
    wp_enqueue_script('js-cookie', get_template_directory_uri() . '/js/js.cookie.js', array('jquery'), '1.00', true);
    wp_enqueue_script('total-custom', get_template_directory_uri() . '/js/total-custom.js', array('jquery'), '1.00', true);

    $page_overwrite_defaults = '';
    if (is_singular(array('post', 'page', 'product', 'portfolio'))) {
        $page_overwrite_defaults = rwmb_meta('page_overwrite_defaults');
    }

    if (!$page_overwrite_defaults) {
        $title_padding = get_theme_mod('total_plus_titlebar_padding', 50);
    } else {
        $title_padding = rwmb_meta('titlebar_padding');
    }

    $is_rtl = (is_rtl()) ? 'true' : 'false';

    $is_customize_preview = (is_customize_preview()) ? 'true' : 'false';

    wp_localize_script('total-custom', 'total_options', array(
        'template_path' => get_template_directory_uri(),
        'rtl' => $is_rtl,
        'customize_preview' => $is_customize_preview,
        'enable_nice_scroll' => get_theme_mod('total_plus_nice_scroll', false),
        'skin_color' => get_theme_mod('total_plus_template_color', '#FFC107'),
        'title_padding' => $title_padding
            )
    );

    wp_localize_script('total-megamenu', 'total_megamenu', array(
        'rtl' => $is_rtl
            )
    );

    wp_enqueue_style('total-style', get_stylesheet_uri(), array('ht-loaders'), '1.00');
    wp_style_add_data('total-style', 'rtl', 'replace');
    wp_enqueue_style('total-fonts', total_plus_fonts_url(), array(), '1.00');
    wp_enqueue_style('font-awesome-5.2.0', get_template_directory_uri() . '/css/all.css', array(), '5.2.0');
    wp_enqueue_style('essential-icon', get_template_directory_uri() . '/css/essential-icon.css', array(), '1.00');
    wp_enqueue_style('materialdesignicons', get_template_directory_uri() . '/css/materialdesignicons.css', array(), '1.00');
    wp_enqueue_style('ht-loaders', get_template_directory_uri() . '/css/loaders.css', array(), '1.00');
    wp_enqueue_style('animate', get_template_directory_uri() . '/css/animate.css', array(), '1.00');
    wp_enqueue_style('icofont', get_template_directory_uri() . '/css/icofont.css', array(), '1.00');
    wp_enqueue_style('owl-carousel', get_template_directory_uri() . '/css/owl.carousel.css', array(), '1.00');
    wp_enqueue_style('lightgallery', get_template_directory_uri() . '/css/lightgallery.css', array(), '1.00');
    wp_enqueue_style('slick', get_template_directory_uri() . '/css/slick.css', array(), '1.00');
    wp_enqueue_style('YTPlayer', get_template_directory_uri() . '/css/jquery.mb.YTPlayer.min.css', array(), '1.00');
    wp_enqueue_style('flipster', get_template_directory_uri() . '/css/jquery.flipster.css', array(), '1.00');
    wp_enqueue_style('jquery-mcustomscrollbar', get_template_directory_uri() . '/css/jquery.mCustomScrollbar.css', array(), '1.00');
    wp_enqueue_style('justifiedGallery', get_template_directory_uri() . '/css/justifiedGallery.min.css', array(), '1.00');
    wp_enqueue_style('slimmenu', get_template_directory_uri() . '/css/slimmenu.css', array(), '1.00');

    if ('file' != get_theme_mod('total_plus_style_option', 'head')) {
        wp_add_inline_style('total-style', total_plus_dymanic_styles());
    } else {

        // We will probably need to load this file
        require_once( ABSPATH . 'wp-admin' . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'file.php' );

        global $wp_filesystem;
        $upload_dir = wp_upload_dir(); // Grab uploads folder array
        $dir = trailingslashit($upload_dir['basedir']) . 'total-plus' . DIRECTORY_SEPARATOR; // Set storage directory path

        WP_Filesystem(); // Initial WP file system
        $wp_filesystem->mkdir($dir); // Make a new folder 'oceanwp' for storing our file if not created already.
        $wp_filesystem->put_contents($dir . 'custom-style.css', total_plus_dymanic_styles(), 0644); // Store in the file.
        wp_enqueue_style('total-dynamic-style', trailingslashit($upload_dir['baseurl']) . 'total-plus/custom-style.css', array(), '1.00');
    }

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'total_plus_scripts');

/**
 * BreadCrumb
 */
require get_template_directory() . '/inc/breadcrumbs.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/total-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer/customizer.php';

/**
 * Custom PostType additions.
 */
require get_template_directory() . '/inc/total-custom-post-types.php';

/**
 * MetaBox additions.
 */
require get_template_directory() . '/inc/total-metabox.php';

/**
 * FontAwesome Array
 */
require get_template_directory() . '/inc/font-icons.php';

/**
 * Typography
 */
require get_template_directory() . '/inc/typography/typography.php';

/**
 * Menu Icons
 */
if (!class_exists('Menu_Icons')) {
    require get_template_directory() . '/inc/assets/menu-icons/menu-icons.php';
}

/**
 * Theme Settings
 */
require get_template_directory() . '/inc/total-plus/welcome.php';

/**
 * Widgets
 */
require get_template_directory() . '/inc/widgets/widgets.php';

/**
 * Header Functions
 */
require get_template_directory() . '/inc/header/header-functions.php';

/**
 * Home Page Functions
 */
require get_template_directory() . '/inc/home-functions.php';

/**
 * Hooks
 */
require get_template_directory() . '/inc/total-hooks.php';

/**
 * Woo Commerce Functions
 */
require get_template_directory() . '/inc/woo-functions.php';

/**
 * AriColor
 */
require get_template_directory() . '/inc/aricolor.php';

/**
 * MetaBox
 */
require get_template_directory() . '/inc/assets/meta-box/meta-box.php';
require get_template_directory() . '/inc/assets/meta-box-columns/meta-box-columns.php';
require get_template_directory() . '/inc/assets/meta-box-tabs/meta-box-tabs.php';
require get_template_directory() . '/inc/assets/meta-box-conditional-logic/meta-box-conditional-logic.php';
require get_template_directory() . '/inc/assets/meta-box-group/meta-box-group.php';

/**
 * Menu Walker
 */
require get_template_directory() . '/inc/walker/init.php';
require get_template_directory() . '/inc/walker/menu-walker.php';

/**
 * Elementor Elements
 */
if (defined('ELEMENTOR_VERSION')) {
    require get_template_directory() . '/inc/elementor/jet-elements.php';
}

/**
 * Dynamic Styles additions
 */
require get_template_directory() . '/inc/style.php';

/**
 * Image Resize
 */
//require get_template_directory() . '/inc/aq_resizer.php';
